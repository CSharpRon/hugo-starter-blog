# Hugo Starter Blog
Welcome to the start of your own static site! With the power of [Hugo's](https://gohugo.io) blazing fast framework for creating websites, you can focus on what matters most: content!  

Fork this repository, pick your favorite name for it and let's get started!

## What you will need

- Gitlab Account (of course!)
- [Gitpod.io Account](https://gitpod.io) (Don't worry, it's free for public repos for 50 hours/month which is plenty)

## What the end result should look like 
![Website Screenshot](website.png)   

## A Note before we dive in
This setup only needs to happen once. After that, you can even create new posts on your phone from the Gitlab app if you are handy with markdown. Several things are taken care of so that you can get up and running quicker:
- The repository includes a Gitlab action to automatically deploy your website each time you commit a change
- Gitpod is used so you do not need to install any software on your machine
- There's this handy document to help you along the way   

Let's get started!
---   

## Setup Checklist
To help you keep track of where you are, follow along with this checklist (sorry you can't just click these boxes, but check each of these off in your head):

- [x] [Discover water on the Moon](https://www.usatoday.com/story/news/nation/2020/10/26/nasa-announce-an-exciting-new-discovery-moon-monday/6039412002/)
- [ ] [1. Fork this repository](#task-1-access)
- [ ] [2. Open the repository in Gitpod](#task-2-launch-gitpod)
- [ ] [3. Modify the config.toml from Gitpod](#task-3-modify-the-hugo-config)
- [ ] [4. Add your first post from Gitpod!](#final-task-add-you-first-post)

### Task 0: Access
Of course, make sure you are logged in to Gitlab and have logged in to [Gitpod](https://gitpod.io) at this point.   
   
### Task 1: Forking
Easy! Just click the little *Fork* button on the top right of this repository.   

### Task 2: Launch Gitpod
Back to the repository. 
- Go to the URL portion of your browser and modify the URL to insert <u>gitpod.io/#</u> just before the word Gitlab.
- It should now look something like: https://gitpod.io/#Gitlab.com/AstroTester/hugo-starter-blog
- Go to that link which will open a new Gitpod instance

### Task 3: Modify the Hugo Config
From Gitpod, find the config.toml file and click it. Replace the following values and make sure to preserve double quotes where they exist:
- baseURL: replace *CSharpRon* with your username, baseURL: "https://csharpron.gitlab.io/hugo-starter-blog/"
- title: set a title for your website
- author -> name: Type in your name!
- languages -> languages.en -> title: Type in your title again
- languages -> languages.en -> subtitle: Type in a witty subtitle

When you are done, click on the *Source Control* button on the left (looks like a Y under the magnifying glass).
- Hover over the word *Changes* and click the + symbol
- In the *Message* field add a short summary of the changes you just made. e.g., "Customized my config"
- Click the *Checkmark* above the message field to save your changes
- Click on the *Synchronize* button at the very bottom to push your changes to the server. Here is what that button looks like:   
![Git Sync](synchronize.png)
- Wait a minute or two and then check out your website. If you did everything right, you should now be able to access your own website at https://your_username.gitlab.io/hugo-starter-blog/

### Final Task: Add you first post!
Now for the fun part and really the only part you need to remember once you are done :)   

- First, reopen the repository in Gitpod.  
- Next, type in the Hugo command in the terminal to create a new post in content/posts: `hugo new posts/first-blog-post.md`

> Taking a look at this command: `hugo new` will create a new file in the path you give it.  
> - The new file will live in the content folder  
> 
> `posts` puts your new post in the content -> posts folder which is a good idea so that your posts are all together.
> `first-blog-post.md` gives your post a name. Note, you must include the .md extension so that Hugo knows it is a Markdown file.

Next, go ahead and fire up the hugo server again so that you can see your edits in realtime. From the terminal execute `hugo server`.

Now, find and open your new blog post in the editor. Using [Markdown syntax](https://guides.Gitlab.com/pdfs/markdown-cheatsheet-online.pdf), write your post.
You can view your post changes in the web editor to make sure that the formatting is correct.
If you want the post to be live on the website, change the "draft" flag on the file to false and commit the change.
That's it, you just published your first blog post! The world is your oyster and all that jazz.
